# AutoSD Distro

## Building

```sh
kas build kas/autosd-qemux86_64.yml
```

## License

[MIT](./LICENSE)
